package ru.vartanyan.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractEntityGraph {

    @Id
    @Nullable
    public String id = UUID.randomUUID().toString();

}
