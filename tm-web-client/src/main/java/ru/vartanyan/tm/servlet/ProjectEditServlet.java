package ru.vartanyan.tm.servlet;

import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.repository.ProjectRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/edit/*")
public class ProjectEditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        req.setAttribute("view_name", "EDIT PROJECT");
        req.setAttribute("project", ProjectRepository.getInstance().findById(id));
        req.getRequestDispatcher("/WEB-INF/views/project-edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String id = req.getParameter("id");
        final String name = req.getParameter("name");
        final String description = req.getParameter("description");
        Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        ProjectRepository.getInstance().save(project);
        resp.sendRedirect("/projects");
    }

}
