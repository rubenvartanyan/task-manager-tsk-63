<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<h1>TASK LIST</h1>

<table width="100%" cellpadding="10" border="1" style="margin-top: 20px">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Description</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    <c:forEach var="task" items="${tasks}">
        <tr>
            <td>
                <c:out value="${task.id}"/>
            </td>
            <td>
                <c:out value="${task.name}"/>
            </td>
            <td>
                <c:out value="${task.description}"/>
            </td>
            <td>
                <a href="/task/edit/?id=${task.id}">Edit</a>
            </td>
            <td>
                <a href="/task/delete/?id=${task.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>

<td>
    <form action="/task/create">
        <button type="submit">CREATE</button>
    </form>
</td>

<jsp:include page="../include/_footer.jsp"/>