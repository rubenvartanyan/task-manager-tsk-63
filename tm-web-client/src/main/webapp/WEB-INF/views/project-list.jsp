<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp"/>
<h1>project LIST</h1>

<table width="100%" cellpadding="10" border="1" style="margin-top: 20px">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Description</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            </td>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td>
                <a href="/project/edit/?id=${project.id}">Edit</a>
            </td>
            <td>
                <a href="/project/delete/?id=${project.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>

<td>
    <form action="/project/create">
        <button type="submit">CREATE</button>
    </form>
</td>

<jsp:include page="../include/_footer.jsp"/>