package ru.vartanyan.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractProjectListener;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.util.TerminalUtil;

@Component
public class ProjectRemoveByIndexListener extends AbstractProjectListener {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-remove-by-index";
    }

    @Override
    public String description() {
        return "Remove project by index";
    }

    @Override
    @EventListener(condition = "@projectRemoveByIndexListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        @Nullable final SessionDTO session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        System.out.println("[REMOVE PROJECT");
        System.out.println("[ENTER INDEX");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        projectEndpoint.removeProjectByIndex(index, session);
        System.out.println("[PROJECT REMOVED]");
    }

}
